package Helper;

import Visitors.Visitor;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class CFile {
    public String directoryFullName;
    public String fileName;
    public String content;
    public ArrayList<String> lines;

    public CFile(String fileName, String directoryFullName){
        this.fileName = fileName;
        this.directoryFullName = directoryFullName;
        lines = new ArrayList<String>();
        content = "";
    }

    public boolean writeLinesToFile(){
        if(!isDirectoryExist())
            return false;
        String fullFileName = directoryFullName + "/" + fileName;
        try (PrintWriter p = new PrintWriter(new FileOutputStream(fullFileName))){
            p.println(Globals.contentThatDoesnotBelongToSpecificCFile);
            p.println(content);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return true;
    }

    private boolean isDirectoryExist() {
        Path targetDirectoryPath = Paths.get(this.directoryFullName);
        if(Files.exists(targetDirectoryPath)){
            return true;
        }
        return false;
    }
}
