package Helper;

import Visitors.CFileVisitor;
import Visitors.MachineVisitor;
import org.w3c.dom.Document;

public class Globals {
    public static String targetCProjectDirectory = "";
    public static CFileVisitor curentCFileVisitor;
    public static MachineVisitor currentMachineVisitor;
    public static Document doc;
    public static String contentThatDoesnotBelongToSpecificCFile = "";
}
