package Visitors;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import Helper.CFile;
import Helper.Globals;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class CFileVisitor extends Visitor {

    public CFileVisitor(Element xmlElement){
        super(xmlElement);
    }

    public CFile cFile;

    @Override
    protected boolean enter() {
        Globals.curentCFileVisitor = this;

        String cFileName = getCFileName();
        cFile = new CFile(cFileName, Globals.targetCProjectDirectory);
        return true;
    }

    private String getCFileName() {
        Element xmlElement = getXmlElement();
        if(xmlElement == null)
            return "";
        return xmlElement.getAttribute("name");
    }

    @Override
    protected boolean exit()
    {
        cFile.writeLinesToFile();
        Globals.curentCFileVisitor = null;
        return true;
    }
}
