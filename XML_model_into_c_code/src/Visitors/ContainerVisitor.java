package Visitors;

import Helper.Globals;
import org.w3c.dom.Element;

import java.sql.SQLOutput;

public class ContainerVisitor extends Visitor   {

    public ContainerVisitor(Element xmlElement) {
        super(xmlElement);
    }

    @Override
    protected boolean enter() {
        if(shouldNotPrintAnyParansAndJustVisitChildren())
            return true;

        String contentsBeforeOpenBracket = getContentsBeforeOpenBracket();
        String content = contentsBeforeOpenBracket + " {\n";
        writeContentToFile(content);
        return true;
    }

    private boolean shouldNotPrintAnyParansAndJustVisitChildren() {
        if(this.getAttributeValueForXmlElement("type").equals("if"))//if container has then and else containers
            return true;
        if(Globals.curentCFileVisitor == null)
            return true;
        if(Globals.currentMachineVisitor == null)
            return true;
        String currentMachineName = Globals.currentMachineVisitor.getAttributeValueForXmlElement("name");
        if(currentMachineName.equals(MachineVisitor.GLOBAL_MACHINE_NAME))
            return true;

        return false;
    }

    //If there is any content before the open bracket then the child element has to implement this function
    protected String getContentsBeforeOpenBracket() {
        return "";
    }

    @Override
    protected boolean exit() {
        if(shouldNotPrintAnyParansAndJustVisitChildren())
            return true;

        String content = "}\n";
        writeContentToFile(content);
        return true;
    }

    public static ContainerVisitor getContainerVisitorAccordingToType(Element childElement) {
        ContainerVisitor containerVisitor = null;
        String type = childElement.getAttribute("type");
        switch (childElement.getAttribute("type")){
            case "then":
                containerVisitor = new ThenContainerVisitor(childElement);
                break;
            case "else":
                containerVisitor = new ElseContainerVisitor(childElement);
                break;
            case "loop":
                containerVisitor = new LoopContainerVisitor(childElement);
                break;
            default:
                containerVisitor = new ContainerVisitor(childElement);
        }
        return containerVisitor;
    }
}
