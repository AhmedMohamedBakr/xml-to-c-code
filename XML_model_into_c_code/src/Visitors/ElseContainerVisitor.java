package Visitors;

import org.w3c.dom.Element;

public class ElseContainerVisitor extends ContainerVisitor  {

    public ElseContainerVisitor(Element xmlElement) {
        super(xmlElement);
    }

    @Override
    protected String getContentsBeforeOpenBracket() {
        String content = "else";
        return content;
    }
}
