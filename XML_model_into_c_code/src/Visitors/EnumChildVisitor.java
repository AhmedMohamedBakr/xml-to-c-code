package Visitors;

import org.w3c.dom.Element;

public class EnumChildVisitor extends Visitor {

    public EnumChildVisitor(Element xmlElement) {
        super(xmlElement);
    }

    @Override
    protected boolean enter() {
        String childName = getAttributeValueForXmlElement("name");
        String childVal = getAttributeValueForXmlElement("val");
        String content = childName + " : "+ childVal +",\n";
        writeContentToFile(content);
        return true;
    }
}
