package Visitors;

import org.w3c.dom.Element;

public class EnumNodeVisitor extends Visitor {

    public EnumNodeVisitor(Element xmlElement) {
        super(xmlElement);
    }

    @Override
    protected boolean enter() {
        String enumName = getAttributeValueForXmlElement("name");
        String content = enumName + " {\n";
        writeContentToFile(content);
        return true;
    }

    @Override
    protected boolean exit() {
        String content = "};\n";
        writeContentToFile(content);
        return true;
    }
}
