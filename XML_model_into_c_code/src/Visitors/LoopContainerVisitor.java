package Visitors;

import org.w3c.dom.Element;

public class LoopContainerVisitor extends ContainerVisitor {

    public LoopContainerVisitor(Element xmlElement) {
        super(xmlElement);
    }

    @Override
    protected String getContentsBeforeOpenBracket() {
        String loopCondition = getAttributeValueForXmlElement("condition");
        String content = "while(" + loopCondition + ")";
        return content;
    }
}
