package Visitors;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class MachineListVisitor extends Visitor {

    public MachineListVisitor(Element xmlElement){
        super(xmlElement);
    }

    @Override
    protected boolean enter() {
        Element xmlElement = getXmlElement();
        System.out.println("Node name is " + xmlElement.getNodeName());
        return true;
    }

    @Override
    protected boolean exit() {
        return false;
    }
}
