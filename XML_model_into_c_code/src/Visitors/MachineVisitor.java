package Visitors;

import Helper.Globals;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.util.ArrayList;

public class MachineVisitor extends Visitor  {
    public static String GLOBAL_MACHINE_NAME = "AMAN_GLOBAL_MACHINE";
    private String content = "";
    private String machineName;

    public ArrayList<String> parametersList = new ArrayList<String>();
    public MachineVisitor(Element xmlElement){
        super(xmlElement);
    }

    @Override
    protected boolean enter() {
        Globals.currentMachineVisitor = this;
        machineName = getAttributeValueForXmlElement("name");
        if(machineName.equals(GLOBAL_MACHINE_NAME))
            return true;

        String machinereturnType = getAttributeValueForXmlElement("returnType");
        String parametersListStr = getParametersListStr();
        String content = machinereturnType + " " + machineName + "(" + parametersListStr + ")";
        writeContentToFile(content);
        return true;
    }

    private String getParametersListStr() {
        String parametersListStr = "";
        String expression = "/machineList/files/cFile/machines/machine[@name = '"+this.machineName+"']/container[@type = 'function']/variableInitializations/variableInitializationNode";
        XPath xPath = XPathFactory.newInstance().newXPath();
        NodeList nodeList = null;
        try {
            nodeList = (NodeList) xPath.compile(expression).evaluate(
                    Globals.doc, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        if(nodeList == null)
            return "";
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nNode = nodeList.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                if(!eElement.getNodeName().equals("variableInitializationNode"))
                    continue;
                if(!eElement.getAttribute("isFuncParam").equals("true"))
                    continue;
                String varType = eElement.getAttribute("type");
                String varName = eElement.getAttribute("name");
                String paramVal = varType + " " + varName;
                this.parametersList.add(paramVal);
            }
        }

        for (var child :
                this.parametersList) {
            if(parametersListStr.length() > 0)
                parametersListStr += ",";
            parametersListStr += child;
        }
        return parametersListStr;
    }

    @Override
    protected boolean exit() {
        Globals.currentMachineVisitor = null;
        return false;
    }
}
