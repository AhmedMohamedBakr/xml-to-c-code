package Visitors;

import org.w3c.dom.Element;

public class Structure_UnionChildVisitor extends Visitor {

    public Structure_UnionChildVisitor(Element xmlElement) {
        super(xmlElement);
    }

    @Override
    protected boolean enter() {
        String childType = getAttributeValueForXmlElement("type");
        String childName = getAttributeValueForXmlElement("name");
        String content = childType + " " + childName + ";\n";
        writeContentToFile(content);
        return true;
    }
}
