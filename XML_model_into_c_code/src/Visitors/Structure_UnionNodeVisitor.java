package Visitors;

import org.w3c.dom.Element;

public class Structure_UnionNodeVisitor extends Visitor {

    public Structure_UnionNodeVisitor(Element xmlElement) {
        super(xmlElement);
    }

    @Override
    protected boolean enter() {
        String structName = getAttributeValueForXmlElement("name");
        String content = structName + " {\n";
        writeContentToFile(content);
        return true;
    }

    @Override
    protected boolean exit() {
        String content = "};\n";
        writeContentToFile(content);
        return true;
    }
}
