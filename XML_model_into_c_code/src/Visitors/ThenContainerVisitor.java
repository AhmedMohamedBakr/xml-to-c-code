package Visitors;

import org.w3c.dom.Element;

public class ThenContainerVisitor extends ContainerVisitor {
    public ThenContainerVisitor(Element xmlElement) {
        super(xmlElement);
    }

    @Override
    protected String getContentsBeforeOpenBracket() {
        String thenCondition = this.getAttributeValueForXmlElement("condition");
        String content = "if("+thenCondition+")";
        return content;
    }
}
