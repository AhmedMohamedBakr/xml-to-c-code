package Visitors;

import org.w3c.dom.Element;

public class TransitionVisitor extends Visitor {

    public TransitionVisitor(Element xmlElement) {
        super(xmlElement);
    }

    @Override
    protected boolean enter() {
        String transitionCommand = getAttributeValueForXmlElement("command");
        transitionCommand = translateAmanDefaultKeywordsIfExists(transitionCommand);
        if(transitionCommand.length() > 0) {
            String content = transitionCommand + ";\n";
            writeContentToFile(content);
        }
        return false;
    }

    private String translateAmanDefaultKeywordsIfExists(String transitionCommand) {
        String retStr = "";
        retStr = transitionCommand.replaceFirst("AMAN_RETURN_CMD", "return ");
        return retStr;
    }
}
