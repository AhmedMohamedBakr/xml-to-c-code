package Visitors;

import org.w3c.dom.Element;

public class TypeDefVisitor extends Visitor {

    public TypeDefVisitor(Element xmlElement) {
        super(xmlElement);
    }

    @Override
    protected boolean enter() {
        String oldTypeName = getAttributeValueForXmlElement("translatedTypeName");
        String newTypeName = getAttributeValueForXmlElement("newTypeName");
        String content = "typedef " + oldTypeName + " "+ newTypeName +";\n";
        writeContentToFile(content);
        return true;
    }
}
