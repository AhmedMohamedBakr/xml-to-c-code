package Visitors;

import Helper.Globals;
import org.w3c.dom.Element;

public class VarInitVisitor extends Visitor  {
    private String varType;
    private String varName;
    public VarInitVisitor(Element xmlElement) {
        super(xmlElement);
    }

    @Override
    protected boolean enter() {

        varType = getAttributeValueForXmlElement("type");
        varName = getAttributeValueForXmlElement("name");

        String isFunctionParameterStr = getAttributeValueForXmlElement("isFuncParam");
        if(isFunctionParameterStr.equals("false"))
            enterWhenVariableIsNotFunctionParameter();
        return false;//do not traverse the children for this node
    }

    private boolean enterWhenVariableIsNotFunctionParameter() {
        if(isInitializationNodeForStructOrUnionChildElement())
            return false;
        if(isInitializationNodeForArrayElement())
            return false;
        String parameterVal = this.varType + " " + this.varName;
        if(isExternGlobalVar())
            parameterVal = "extern " + parameterVal;
        String content = parameterVal + ";\n";
        writeContentToFile(content);
        return false;
    }

    private boolean isInitializationNodeForArrayElement() {
        return this.varName.contains("[") && this.varName.contains("]");
    }

    private boolean isInitializationNodeForStructOrUnionChildElement() {
        return this.varName.contains(".");
    }

    private boolean isExternGlobalVar() {
        return Globals.curentCFileVisitor == null;
    }

    @Override
    protected boolean exit() {
        return false;
    }
}
