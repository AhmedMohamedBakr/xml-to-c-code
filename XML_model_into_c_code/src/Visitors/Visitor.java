package Visitors;

import Helper.Globals;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;

public class Visitor {

    private Element xmlElement = null;

    public Visitor(Element xmlElement){
        this.xmlElement = xmlElement;
    }

    protected boolean enter(){
        return true;
    }

    protected boolean exit(){
        return false;
    }

    public Element getXmlElement() {
        return xmlElement;
    }

    public Visitor traverseElement(){
        return traverseElement(this.xmlElement);
    }

    public static Visitor traverseElement(Element xmlElement){
        if(xmlElement == null)
            return null;

        Visitor visitor = getVisitorAccordingToChildType(xmlElement);
        if(visitor.enter())
            visitor.traverseChildrenElements();
        visitor.exit();

        return visitor;
    }

    private static Visitor getVisitorAccordingToChildType(Element childElement){
        Visitor visitor = null;
        switch (childElement.getNodeName()){
            case "machineList":
                visitor = new MachineListVisitor(childElement);
                break;
            case "files":
                visitor = new FilesVisitor(childElement);
                break;
            case "cFile":
                visitor = new CFileVisitor(childElement);
                break;
            case "machine":
                visitor = new MachineVisitor(childElement);
                break;
            case "variableInitializationNode":
            case "PointerInitializationNode":
            case "ArrayInitializationNode":
                visitor = new VarInitVisitor(childElement);
                break;
            case "container":
                visitor = ContainerVisitor.getContainerVisitorAccordingToType(childElement);
                break;
            case "transition":
                visitor = new TransitionVisitor(childElement);
                break;
            case "structureNode":
            case "unionNode":
                visitor = new Structure_UnionNodeVisitor(childElement);
                break;
            case "structureChild":
            case "unionChild":
                visitor = new Structure_UnionChildVisitor(childElement);
                break;
            case "enumNode":
                visitor = new EnumNodeVisitor(childElement);
                break;
            case "enumChild":
                visitor = new EnumChildVisitor(childElement);
                break;
            case "typedef":
                visitor = new TypeDefVisitor(childElement);
                break;
            default:
                visitor = new Visitor(childElement);
        }
        return visitor;
    }

    public boolean traverseChildrenElements(){
        ArrayList<Element> childrenElements = getListOfChildrenElements(xmlElement);

        for (Element childElement: childrenElements) {
            Visitor visitor = getVisitorAccordingToChildType(childElement);
            if(visitor == null)
                continue;

            visitor.traverseElement();
        }
        return true;
    }

    public String getAttributeValueForXmlElement(String inlineAttributeName){
        if(xmlElement == null)
            return "";
        return xmlElement.getAttribute(inlineAttributeName);
    }

    public static void findAllElementsWithSpecificXmlNodeName(String nodeName, Element parentElement,
                                                              ArrayList<Element> foundElements){
        if(parentElement.getNodeName().equals(nodeName))
            foundElements.add(parentElement);
        ArrayList<Element> childrenElements = getListOfChildrenElements(parentElement);

        for (var child :
                childrenElements) {
            findAllElementsWithSpecificXmlNodeName(nodeName, child, foundElements);
        }
    }

    public static ArrayList<Element> getListOfChildrenElements(Element parentElement){
        if(parentElement == null)
            return null;

        ArrayList<Element> childrenElements = new ArrayList<>();
        NodeList nodeList = parentElement.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nNode = nodeList.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                childrenElements.add(eElement);
            }
        }
        return childrenElements;
    }

    public boolean writeContentToFile(String content){
        if(Globals.curentCFileVisitor == null)
            Globals.contentThatDoesnotBelongToSpecificCFile += content;
        else
            Globals.curentCFileVisitor.cFile.content += content;
        System.out.print(content);
        return true;
    }
}
