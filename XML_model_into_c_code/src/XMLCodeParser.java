import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import Helper.Globals;
import Visitors.MachineListVisitor;
import Visitors.Visitor;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class XMLCodeParser {
    private String xmlFileFullName;
    private String generatedOutputDirectory;
    private boolean isLoadedSuccessfully;

    public XMLCodeParser(String xmlFileFullName, String generatedOutputDirectory){
        this.xmlFileFullName = xmlFileFullName;
        this.generatedOutputDirectory = generatedOutputDirectory;

        Globals.targetCProjectDirectory = generatedOutputDirectory;
    }

    public boolean parse(){
        try {
            File inputFile = new File(xmlFileFullName);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder;

            dBuilder = dbFactory.newDocumentBuilder();

            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            Globals.doc = doc;

            isLoadedSuccessfully = true;
            XPath xPath = XPathFactory.newInstance().newXPath();

            String expression = "/machineList";
            NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(
                    doc, XPathConstants.NODESET);
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node nNode = nodeList.item(i);
                System.out.println("\nCurrent Element :" + nNode.getNodeName());
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    MachineListVisitor machineListVisitor = new MachineListVisitor(eElement);
                    machineListVisitor.traverseElement();
                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        return isLoadedSuccessfully;
    }

    public String getGeneratedOutputDirectory() {
        return generatedOutputDirectory;
    }
}
